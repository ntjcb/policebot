# PoliceBot  
Discord punishment bot developed by notjacob (notjacob#9999)
***
# Purpose
- Logs ips to track users through alt accounts since discord is not good about that
***
# Using it yourself
- clone this repository to your computer
- Run the command `npm install`
- Open the `src` folder and create a file named `secret.json`. This file should look like this: ```
{
    "firebase": {
        Your firebase credentials
    },
    "discordtoken": "your discord bot token"
}```
- Run the command `npm run start`
- The bot should now be running! Follow typical bot setup guides to add it to your server
***
I understand that this might not be ethical without properly letting the user know that their ip is being stored. I will not be using it in production myself and anyone who does understands that there is always a privacy concern when logging people's sensitive data