import firebase from 'firebase'
import * as discord from 'discord.js'
import * as config from './secret.json'
import * as fs from 'fs'
firebase.initializeApp(config.firebase)
require('./webserver/server')
export const client = new discord.Client()
export const db = firebase.firestore()
export const guilds: Array<Guild> = []
const commands: Array<Command> = []
const registerCommands = () => {
    fs.readdirSync(__dirname + "/commands/").filter(str => str.endsWith(".js")).forEach(str => {
        commands.push(require(__dirname + "/commands/" + str.substr(0, str.length - 3)).instance())
    })
}
registerCommands()

export interface Command {
    getInfo(): CommandInfo
    execute(message: discord.Message): Promise<void>
}
export type CommandInfo = {
    name: string,
    permission: discord.PermissionResolvable,
    description?: string,
    usage?: string
}

export enum LimitMode {
    RoleAfter = "ROLEAFTER",
    RoleBefore = "ROLEBEFORE"
}

export type Guild = {
    limitmode: LimitMode,
    gid: number,
    role: number
}


export const loadGuild = async (id: number): Promise<Guild> => {
    return new Promise(async (res, rej) => {
        const g = await db.collection("servers").where("gid", "==", id)
        .get()
        if (g.size > 0) {
            const newg = {limitmode: LimitMode[g.docs[0].get("limitmode")],role:g.docs[0].get("role"),gid:id}
            guilds.push(newg)
            res(newg)
        } else {
            const newg: Guild = {
                limitmode: LimitMode.RoleAfter,
                role: 0,
                gid: id
            }
            db.collection("servers").add(newg)
            guilds.push(newg)
            res(newg)
        }
    })
}

client.on('ready', () => {
    console.log(`Logged in as ${client.user?.tag}`)
})
client.on("message", msg => {
    if (guilds.filter(g => g.gid == Number(msg.guild?.id)).length == 0) {
        loadGuild(Number(msg.guild?.id)).then(_ => {
            getCommand(msg)
        })
    } else {
        getCommand(msg)
    }
})
const getCommand = async (msg: discord.Message) => {
    const start = msg.content.split(" ")[0].substr(1)
    if (msg.content.startsWith("?")) {
        const filtered = commands.filter(cmd => cmd.getInfo().name == start)
        if (filtered.length > 0) {
            filtered[0].execute(msg)
        }
    }
}
client.login(config.discordtoken)
