import express from 'express'
import url from 'url'
import { db } from '../index'

export enum HtmlPage {
    INVALIDKEY = "public/invalid_key.html",
    UNDEFINEDUSER = "public/undefined_user.html",
    HOME = "public/home.html",
    VERIFIED = "public/verified.html"
}

const app = express()
app.use(express.static("./public/"))
app.get('/', (req, res) => {
    res.sendFile("./" + HtmlPage.HOME, {root: "./"})
})
app.get('/handle-id', (req, res) => {
   const key = url.parse(req.url, true).query.num
   const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress
   if (!isNaN(Number(key))) {
        handle(Number(key), ip).then(page => {
            res.sendFile("./" + page, {root: "./"})
        })
   } else {
        res.sendFile("./" + HtmlPage.INVALIDKEY, {root: "./"})
   }
})
app.listen(7000, "0.0.0.0")
const handle = async (key: number, ip: string | string[] | undefined): Promise<HtmlPage> => {
    const doc = await db.collection("sessions").where("code", "==", key)
    .get()
    if (doc.size > 0) {
        const userid: string = doc.docs[0].get("user")
        if (userid) {
            console.log(`IP => ${ip}, USER => ${userid}, CODE => ${key}`)
            return HtmlPage.VERIFIED
            // VERIFY USER AND LOG INFO
        } else {
            return HtmlPage.UNDEFINEDUSER
        }
    }
    return HtmlPage.INVALIDKEY

}