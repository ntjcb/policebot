import { Message } from 'discord.js';
import { Command, CommandInfo } from '../index'

export const instance = (): HelloWorld => {
    return new HelloWorld()
}
export class HelloWorld implements Command {
    getInfo(): CommandInfo {
        return { name: "hello", permission: "SEND_MESSAGES" }
    }
    async execute(message: Message): Promise<void> {
        message.reply("Hey")
    }
    
}